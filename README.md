# ks8 nginx

## create pods

```
kubectl create -f nginx_pod.yaml
kubectl get pods
```

## create service

```
kubectl create -f nginx_service.yaml
kubectl get services
```

## update - increase replicas

```
kubectl apply -f nginx_pod.yaml
kubectl get pods
```
